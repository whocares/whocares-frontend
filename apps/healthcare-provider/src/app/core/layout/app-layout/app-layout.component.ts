import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '@whocares/data-auth';
import { PatientService } from '@whocares/data-patient';

@Component({
  selector: 'whocares-layout',
  templateUrl: './app-layout.component.html',
  styleUrls: ['./app-layout.component.scss']
})
export class AppLayoutComponent {

  constructor(private authService: AuthService, private router: Router, private patientService: PatientService) {
  }

  public logout(): void {
    this.patientService.patient = null;
    this.authService.logout();
  }

}

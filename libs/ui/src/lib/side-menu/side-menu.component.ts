import { Component, Input } from '@angular/core';
import { SideMenuItem } from './side-menu-item.interface';

@Component({
  selector: 'whocares-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.scss']
})
export class SideMenuComponent {
  @Input()
  public menuItems: SideMenuItem[];
}

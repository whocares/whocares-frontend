import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppLayoutComponent } from './core/layout/app-layout/app-layout.component';
import { AuthLayoutComponent } from './core/layout/auth-layout/auth-layout.component';
import { ChatComponent } from './features/chat/pages/chat/chat.component';
import { AuthGuard } from '@whocares/data-auth';

const routes: Routes = [
  {
    path: 'auth',
    component: AuthLayoutComponent,
    loadChildren: () => import('./features/auth/auth.module').then((module) => module.AuthModule)
  },
  {
    path: '',
    component: AppLayoutComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'patient-record',
        loadChildren: () => import('./features/patient-record/patient-record.module').then((module) => module.PatientRecordModule)
      },
      {
        path: 'chat',
        component: ChatComponent
      },
      {
        path: '',
        pathMatch: 'full',
        redirectTo: '/patient-record'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}

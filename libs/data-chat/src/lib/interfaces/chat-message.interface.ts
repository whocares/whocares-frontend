export interface ChatMessage {
  id: string;
  userId: string;
  content: string;
  sentAt: string;
  createdAt?: string;
  chat?: Chat;
  name?: string;
}

export interface Chat {
  id: string;
  users: {userId: string, type: string, firstName?: string, lastName?: string}[];
  messages: ChatMessage[];
  lastMessage: ChatMessage;
  createdAt?: string;
  hcpName: string;
  patientName: string;
  [key: string]: any;
}

import { FormGroup, ValidatorFn } from '@angular/forms';

export class ComparisonValidators {
  public static match(field1: string, field2: string): ValidatorFn {
    return (formGroup: FormGroup): { [key: string]: any } | null => {
      if (formGroup.controls[field1] && formGroup.controls[field2]) {
        return formGroup.controls[field1].value !== formGroup.controls[field2].value ? { match: true } : null;
      }

      return null;
    }
  }
}

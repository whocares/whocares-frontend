export interface Type {
  name: string;
  description: string;
}

export const environment = {
  production: true,
  baseUrl: 'https://whocares-hcp.julespeeters.nl/api/v1',
  firebase: {
    apiKey: "AIzaSyAg9eIYl9Uo66o2Yy4gvAC2si63iUOXaPA",
    authDomain: "whocares-healthcare-provider.firebaseapp.com",
    databaseURL: "https://whocares-healthcare-provider.firebaseio.com",
    projectId: "whocares-healthcare-provider",
    storageBucket: "whocares-healthcare-provider.appspot.com",
    messagingSenderId: "769405829218",
    appId: "1:769405829218:web:6a9e99bd1143fbaa35113b"
  }
};

import { AbstractControl } from '@angular/forms';

export function passwordValidator(formControl: AbstractControl): { [key: string]: any } | null {
  if (formControl && formControl.value) {
    return formControl.value.length > 5  ? null : { password: true };
  }
}

import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Chat } from '@whocares/data-chat';
import { HttpClient } from '@angular/common/http';
import { ChatService } from '@whocares/data-chat';
import { filter, shareReplay, switchMap } from 'rxjs/operators';

@Component({
  selector: 'whocares-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit {
  public chat$: Observable<Chat>;
  public chats$: Observable<Chat[]>;
  public showChat = false;

  constructor(private http: HttpClient, private chatService: ChatService) { }

  public ngOnInit(): void {
    this.chats$ = this.chatService.updateChat.asObservable().pipe(switchMap(() => {
      return this.chatService.getAll();
    }));

    this.chat$ = this.chatService.activeChat$.pipe(filter(Boolean), switchMap((chat: Chat) => {
      return this.chatService.getMessages(chat.id)
    }, (chat, messages) => { return { ...chat, messages} }),
      shareReplay(1));
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChatConfig } from './chat-config';

@NgModule({
  imports: [CommonModule],
})
export class DataChatModule {
  public static forRoot(config: {endpoint: string, scope: string}) {
    return {
      ngModule: DataChatModule,
      providers: [
        ChatConfig,
        {
          provide: ChatConfig,
          useValue: config,
        }
      ]
    }
  }
}

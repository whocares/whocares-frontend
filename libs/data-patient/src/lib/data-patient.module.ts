import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DatePatientConfig } from './date-patient-config';

@NgModule({
  imports: [CommonModule],
})
export class DataPatientModule {
  public static forRoot(config: DatePatientConfig): ModuleWithProviders<DataPatientModule> {
    return {
      ngModule: DataPatientModule,
      providers: [
        { provide: DatePatientConfig, useValue: config },
      ],
    }
  }
}

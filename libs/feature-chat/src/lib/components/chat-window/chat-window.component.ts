import { AfterViewChecked, Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { Chat, ChatMessage, WebsocketService } from '@whocares/data-chat';
import { AuthService } from '@whocares/data-auth';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { ChatService } from '../../../../../data-chat/src/lib/services/chat.service';
import { HealthcareProviderService } from '../../../../../data-healthcare-provider/src/lib/services/healthcare-provider.service';
import { FeatureChatConfig } from '../feature-chat.config';

@Component({
  selector: 'whocares-chat-window',
  templateUrl: './chat-window.component.html',
  styleUrls: ['./chat-window.component.scss']
})
export class ChatWindowComponent implements OnInit, AfterViewChecked {
  get chat(): Chat {
    return this._chat;
  }

  @Input()
  set chat(value: Chat) {
    if (value) {
      this._chat = value;
      this.messages = value.messages;
    }
  }

  @ViewChild('messageContainer', { static: false })
  public messagesContainer: ElementRef;
  @Output()
  public onBackClick = new EventEmitter();

  public messages: ChatMessage[];
  public chat$: Observable<Chat>;
  public userId$: Observable<string>;
  public message: string;

  private _chat: Chat;

  constructor(
    public config: FeatureChatConfig,
    private authService: AuthService,
    private websocketService: WebsocketService,
    private chatService: ChatService,
    private hcpService: HealthcareProviderService,
    private http: HttpClient) {
    this.chat$ = this.chatService.activeChat$;
  }

  public ngOnInit(): void {
    this.listenForEvent();
    this.userId$ = this.authService.user$.pipe(map((user) => {
      return user.uid;
    }));
  }

  public ngAfterViewChecked(): void {
    this.scrollToBottom();
  }

  public addChatMessage(userId: string): void {
    this.messages = [
      ...this.messages, {
        id: null,
        content: this.message,
        userId: userId,
        sentAt: new Date().toISOString(),
        createdAt: new Date().toISOString(),
      }];
    this.chatService.addMessage(this.message, this.chat.id).subscribe((message) => {
    });
    this.message = '';
  }

  public scrollToBottom(): void {
    if (this.messagesContainer) {
      this.messagesContainer.nativeElement.scrollTop = this.messagesContainer.nativeElement.scrollHeight;
    }
  }

  public listenForEvent(): void {
    this.chatService.messageAdded$.pipe(filter(Boolean)).subscribe((message: ChatMessage) => {
      this.messages = [...this.messages, message];
    });
  }

  public back(): void {
    this.chatService.setActiveChat(null);
    this.onBackClick.emit();
  }

}

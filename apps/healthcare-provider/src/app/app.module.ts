import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { ReactiveFormsModule } from '@angular/forms';
import { CoreModule } from './core/core.module';
import { DataPatientModule } from '@whocares/data-patient';
import { TokenInterceptor } from '@whocares/data-auth';
import { ChatComponent } from './features/chat/pages/chat/chat.component';
import { FeatureChatModule } from '@whocares/feature-chat';
import { DataChatModule } from '@whocares/data-chat';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { DataHealthcareProviderModule } from '@whocares/data-healthcare-provider';
import { CamelCaseInterceptor } from '../../../patient/src/app/core/interceptors/camel-case.interceptor';
import { ToastModule } from 'primeng/toast';
import { MessageService } from 'primeng/api';
import { environment } from '../environments/environment';

@NgModule({
  declarations: [AppComponent, ChatComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    ReactiveFormsModule,
    CoreModule,
    HttpClientModule,
    DataPatientModule.forRoot({ baseUrl: environment.baseUrl }),
    FeatureChatModule.forRoot({scope: 'health-care-provider'}),
    DataChatModule.forRoot({ endpoint: environment.baseUrl, scope: 'health-care-provider' }),
    DataHealthcareProviderModule.forRoot({ endpoint: environment.baseUrl }),
    ProgressSpinnerModule,
    ToastModule,
  ],
  providers: [
    MessageService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: CamelCaseInterceptor,
      multi: true
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}

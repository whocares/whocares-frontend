export interface RecordCase {
  id: string;
  diagnosisId: string;
  diagnosis?: Diagnoses;
  patientId: string;
  createdAt: string;
  recordElements: RecordElement[];
  appointments?: RecordElement[];
}

export interface RecordElement {
  id: string;
  recordCaseId: string;
  elementType: number;
  content: string;
  createdAt: string;
  medications?: Medication[];
}

export interface Diagnoses {
  id: string;
  name: string;
  description: string;
  createdAt: string;
}

export interface Medication {
  id: string;
  patientId: string;
  medicine: any;
  amount: number;
  usageType: string;
  dosage: string;
  startDate: string;
  endDate: string;
}

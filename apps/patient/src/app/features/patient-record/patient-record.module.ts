import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PatientRecordRoutingModule } from './patient-record-routing.module';
import { RecordComponent } from './record/record.component';
import { TableModule } from 'primeng/table';
import { SharedModule } from '../../shared/shared.module';
import { CardModule } from 'primeng/card';
import { PatientInfoCardComponent } from './components/patient-info-card/patient-info-card.component';
import { RecordCaseDetailComponent } from './pages/record-case-detail/record-case-detail.component';
import {HealthcareproviderOverviewComponent} from "./healthcareprovider-overview/healthcareprovider-overview.component";

@NgModule({
  declarations: [RecordComponent, PatientInfoCardComponent, RecordCaseDetailComponent, HealthcareproviderOverviewComponent],
  imports: [
    CommonModule,
    TableModule,
    SharedModule,
    CardModule,
    PatientRecordRoutingModule
  ]
})
export class PatientRecordModule {
}

import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { environment } from '../environments/environment';
import { ReactiveFormsModule } from '@angular/forms';
import { CoreModule } from './core/core.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { CamelCaseInterceptor } from './core/interceptors/camel-case.interceptor';
import { ChatComponent } from './features/chat/pages/chat/chat.component';
import { FeatureChatModule } from '@whocares/feature-chat';
import { DataChatModule } from '@whocares/data-chat';
import { DataPatientModule } from '@whocares/data-patient';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { ToastModule } from 'primeng/toast';
import { MessageModule } from 'primeng/message';
import { MessageService } from 'primeng/api';
import { TokenInterceptor } from '@whocares/data-auth';
import { DataHealthcareProviderModule } from '@whocares/data-healthcare-provider';

@NgModule({
  declarations: [
    AppComponent,
    ChatComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    ReactiveFormsModule,
    CoreModule,
    HttpClientModule,
    DataChatModule.forRoot({ endpoint: environment.baseUrl, scope: 'patient' }),
    DataPatientModule.forRoot({ baseUrl: environment.baseUrl }),
    DataHealthcareProviderModule.forRoot({ endpoint: environment.baseUrl }),
    FeatureChatModule.forRoot({ scope: 'patient' }),
    ProgressSpinnerModule,
    ToastModule,
    MessageModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: CamelCaseInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    MessageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}

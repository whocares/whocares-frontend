// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  baseUrl: 'http://localhost:8080/api/v1',
  firebase: {
    apiKey: 'AIzaSyBTOIUj54oU_DmfGAJSR1LBmElEajjK7-8',
    authDomain: 'whocares-afb18.firebaseapp.com',
    databaseURL: 'https://whocares-afb18.firebaseio.com',
    projectId: 'whocares-afb18',
    storageBucket: 'whocares-afb18.appspot.com',
    messagingSenderId: '208077733868',
    appId: '1:208077733868:web:6b138f5dcb128f26ef0ac9'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

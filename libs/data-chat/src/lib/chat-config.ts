import { Injectable } from '@angular/core';

@Injectable()
export class ChatConfig {
  public endpoint: string;
  public scope: 'patient' | 'health-care-provider';
}

import { Injectable } from '@angular/core';
import Pusher from 'pusher-js';
import { AuthService } from '@whocares/data-auth';
import { filter, switchMap } from 'rxjs/operators';
import { fromPromise } from 'rxjs/internal-compatibility';
import { ChatConfig } from '../chat-config';

@Injectable({
  providedIn: 'root'
})
export class WebsocketService {
  public pusher: Pusher;

  constructor(private authService: AuthService, private chatConfig: ChatConfig) {
    this.authService.user$.pipe(
      filter(Boolean),
      switchMap((user: any) => fromPromise(user.getIdToken()), (user, token) => [user, token])
    ).subscribe(([user, token]) => {
      this.pusher = new Pusher('69723786452734072d16', {
        cluster: 'eu',
        authEndpoint: `${this.chatConfig.endpoint}/chat-service/broadcast`,
        auth: {
          headers: {
            'Authorization': `Bearer ${token}`
          }
        }
      });

      this.listenToChannel([`private-users.${(user as firebase.User).uid}`]);
    });
  }

  public listenToChannel(channelNames: string[]): void {
    channelNames.forEach((channelName) => {
      this.pusher.subscribe(`${channelName}`);

      Pusher.log = msg => {
        console.log(msg);
      };

      this.pusher.connection.bind('connected', this.afterConnected);
    });

    this.pusher.allChannels().forEach((channel) => {
      console.log('Subscribed to: ' + channel.name);
    });
  }

  public afterConnected(evt: any): void {
    console.log('CONNECTED!', evt);

  }

  public disconnect(channelNames: string[]): void {
    channelNames.forEach((channelName) => {
      this.pusher.unsubscribe(channelName);
    });
  }
}

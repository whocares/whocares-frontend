import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChatWindowComponent } from './components/chat-window/chat-window.component';
import { ChatMessageComponent } from './components/chat-message/chat-message.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ChatListComponent } from './components/chat-list/chat-list.component';
import { HttpClientModule } from '@angular/common/http';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { DropdownModule } from 'primeng/dropdown';
import { AddChatDialogComponent } from './components/add-chat-dialog/add-chat-dialog.component';
import { UiModule } from '@whocares/ui';
import { DynamicDialogModule } from 'primeng/dynamicdialog';
import { ChatConfig } from '../../../data-chat/src/lib/chat-config';
import { FeatureChatConfig } from './components/feature-chat.config';
import { ChatUsersPipe } from './pipes/chat-users.pipe';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    ProgressSpinnerModule,
    DropdownModule,
    UiModule,
    ReactiveFormsModule,
    DynamicDialogModule,
  ],
  declarations: [
    ChatWindowComponent,
    ChatMessageComponent,
    ChatListComponent,
    AddChatDialogComponent,
    ChatUsersPipe,
  ],
  exports: [
    ChatWindowComponent,
    ChatMessageComponent,
    ChatListComponent,
  ]
})
export class FeatureChatModule {
  public static forRoot(config: {scope: string}) {
    return {
      ngModule: FeatureChatModule,
      providers: [
        {
          provide: FeatureChatConfig,
          useValue: config,
        }
      ]
    }
  }
}

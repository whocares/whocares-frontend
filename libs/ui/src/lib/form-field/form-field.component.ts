import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnDestroy,
  ViewEncapsulation
} from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'whocares-form-field',
  templateUrl: './form-field.component.html',
  styleUrls: ['./form-field.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class FormFieldComponent implements OnDestroy {
  @Input()
  public label: string;


  get control(): FormControl {
    return this._control;
  }

  @Input()
  set control(value: FormControl) {
    this._control = value;
    this.sub = value.valueChanges.subscribe(() => this.cdr.detectChanges());

    this.cdr.detectChanges();
  }

  public status$: Observable<string>;

  private _control: FormControl;
  private sub: Subscription;

  constructor(private cdr: ChangeDetectorRef) {
  }

  public ngOnDestroy(): void {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }
}

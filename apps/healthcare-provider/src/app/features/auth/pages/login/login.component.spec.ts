import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { LoginComponent } from './login.component';
import { of, throwError } from 'rxjs';
import { AuthModule } from '../../auth.module';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';
import { AuthService, AuthMockService } from '@whocares/data-auth';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [],
      providers: [{ provide: AuthService, useClass: AuthMockService }],
      imports: [AuthModule, RouterTestingModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call the authService on login', () => {
    const authService = fixture.debugElement.injector.get(AuthService);
    const router = fixture.debugElement.injector.get(Router);
    const loginSpy = spyOn(authService, 'login').and.returnValue(of(null));
    const routerSpy = spyOn(router, 'navigate');

    component.form.setValue({ email: 'dev@dev.dev', password: 'dev' });
    component.login();

    expect(loginSpy).toHaveBeenCalledWith('dev@dev.dev', 'dev');
    expect(routerSpy).toHaveBeenCalledWith(['/']);
  });

  it('should set error property on faulty login', () => {
    const authService = fixture.debugElement.injector.get(AuthService);
    const loginSpy = spyOn(authService, 'login').and.returnValue(throwError('test'));

    expect(component.loginError).toBeFalsy();

    component.login();

    expect(component.loginError).toBe(true);
  });
});

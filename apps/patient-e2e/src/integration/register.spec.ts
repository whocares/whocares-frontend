import faker from 'faker';

const requiredFields = [
  'email',
  'first-name',
  'last-name',
  'password',
  'password-confirmation'
];

describe('Register', () => {
  before(() => localStorage.clear());

  beforeEach(() => cy.visit('/'));

  it('should navigate to the register page when clicking register button', () => {
    cy.get('[data-test=register-btn]').click();

    cy.url().should('include', 'register');
    cy.get('[data-test=register-title]').should('contain', 'Registreer');
  });

  it('should validate the password fields', () => {
    cy.get('[data-test=register-btn]').click();

    cy.get('[data-test=password-error-message]').should('not.exist');

    cy.get('[data-test=password-field]').type('test');
    cy.get('[data-test=password-confirmation-field]').type('not-test');

    cy.get('[data-test=password-error-message]').should('exist');
  });

  it('should show a validation message at required fields', () => {
    cy.get('[data-test=register-btn]').click();

    requiredFields.forEach((field) => {
      cy.get(`[data-test=${field}-field]`).type('test').clear().blur();
      cy.get(`[data-test=${field}-form-field] [data-test=required-validation-message]`).should('exist');
    });
  });

  it('should show an error message when trying to submit an invalid form', () => {
    cy.get('[data-test=register-btn]').click();
    cy.get('[data-test=form-error-message]').should('not.exist');
    cy.get('[data-test=submit-btn]').click();
    cy.get('[data-test=form-error-message]').should('exist');
  });

  // it('should submit a valid form', () => {
  //   // cy.intercept({method: 'POST', url: '/patient-service/patients'}, {data: {id: 1}});
  //   cy.get('[data-test=register-btn]').click();
  //   const email = faker.internet.email();
  //   requiredFields.forEach((field) => {
  //     cy.get(`[data-test=${field}-field]`).type(email);
  //   });
  //
  //   cy.get('[data-test=gender-field] .p-button:first').click();
  //   cy.get('p-calendar').click();
  //   cy.get('p-calendar .p-datepicker-today').click();
  //
  //   cy.get('[data-test=submit-btn]').click();
  //
  //   cy.wait(8000);
  //
  //   cy.url().should('include', 'patient-record');
  // });
});

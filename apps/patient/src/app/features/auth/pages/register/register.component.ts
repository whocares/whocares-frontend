import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SelectItem } from 'primeng/api';
import { switchMap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { ComparisonValidators } from '../../../../shared/validators/comparison.validators';
import { passwordValidator } from '../../../../shared/validators/password.validator';
import { AuthService } from '@whocares/data-auth';
import { PatientService } from '@whocares/data-patient';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  public form: FormGroup;
  public today = new Date();
  public genderOptions: SelectItem[] = [
    { label: 'Man', value: 'M' },
    { label: 'Vrouw', value: 'F' },
  ];

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private patientService: PatientService,
    private router: Router,
  ) {
  }

  public ngOnInit(): void {
    this.buildForm();
  }

  public submit(): void {
    if (this.form.valid) {
      this.authService.register(this.form.value.email, this.form.value.password).pipe(
        switchMap((response) => {
          const patient = { ...this.form.value, firebaseUserId: response.uid };

          return this.patientService.register(patient);
        })).subscribe((response) => {
          localStorage.setItem('patientId', response.data.id);
        this.router.navigate(['/']);
      });
    }
  }

  private buildForm(): void {
    this.form = this.formBuilder.group({
      firstName: [null, Validators.required],
      lastNamePrefix: [null],
      lastName: [null, Validators.required],
      gender: [null, Validators.required],
      email: [null, Validators.compose([Validators.required, Validators.email])],
      birthdate: [null, Validators.required],
      password: [null, Validators.compose([Validators.required, passwordValidator])],
      confirmPassword: [null, Validators.required],
    }, { validators: ComparisonValidators.match('password', 'confirmPassword') });
  }
}

export * from './lib/data-patient.module';
export * from './lib/services/patient.service';
export * from './lib/interfaces/register-request.interface';
export * from './lib/interfaces/record.interface';
export * from './lib/interfaces/patient.interface';

// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  baseUrl: 'http://localhost:8080/api/v1',
  firebase: {
    apiKey: "AIzaSyAg9eIYl9Uo66o2Yy4gvAC2si63iUOXaPA",
    authDomain: "whocares-healthcare-provider.firebaseapp.com",
    databaseURL: "https://whocares-healthcare-provider.firebaseio.com",
    projectId: "whocares-healthcare-provider",
    storageBucket: "whocares-healthcare-provider.appspot.com",
    messagingSenderId: "769405829218",
    appId: "1:769405829218:web:6a9e99bd1143fbaa35113b"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

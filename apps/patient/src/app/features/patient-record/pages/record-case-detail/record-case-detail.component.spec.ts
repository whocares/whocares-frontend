import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecordCaseDetailComponent } from './record-case-detail.component';

describe('RecordCaseDetailComponent', () => {
  let component: RecordCaseDetailComponent;
  let fixture: ComponentFixture<RecordCaseDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecordCaseDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecordCaseDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NestedColumnPipe } from './pipes/nested-column.pipe';

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [
    NestedColumnPipe,
  ],
  exports: [
    NestedColumnPipe,
  ]
})
export class UtilsModule {
}

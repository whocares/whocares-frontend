# wc -l returning more than 1 here means there is at least one pod running
while [ "$(kubectl get pods --field-selector=status.phase=Running -l tier=patient,app=frontend | wc -l)" -le 1 ] ; do
  echo "Waiting for patient frontend to be ready" && sleep 5;
done

echo "RUNNING E2E TESTS"

if npm run ng run patient-e2e:e2e; then
  echo "TESTS SUCCEEDED"
  return 0
else
  echo "TESTS FAILED"
  echo "CURRENT IMAGE IS $NEW_IMAGE_NAME"
  echo "PREVIOUS IMAGE WAS $PREVIOUS_IMAGE_NAME"
  echo "REVERTING DEPLOYMENT TO PREVIOUS IMAGE NAME"

  kubectl set image deployments/"$K8_DEPLOYMENT_NAME" "$K8_CONTAINER_NAME"="$PREVIOUS_IMAGE_NAME"
  return 1
fi

import { Component, OnInit } from '@angular/core';
import { Patient, PatientService, RecordCase } from '@whocares/data-patient';
import { switchMap } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '@whocares/data-auth';

@Component({
  selector: 'whocares-record',
  templateUrl: './record.component.html',
  styleUrls: ['./record.component.css']
})
export class RecordComponent implements OnInit {
  public recordCases: RecordCase[];
  public columns: any[];
  public patient: Patient;

  constructor(
    private patientService: PatientService,
    private router: Router,
    private route: ActivatedRoute,
    private authService: AuthService,
  ) {
  }

  public ngOnInit(): void {

    this.authService.user$.subscribe(async (user) => {
      console.log(await user.getIdToken());
    });

    this.columns = [
      {
        field: 'diagnosis.name',
        header: 'Naam'
      },
      {
        field: 'diagnosis.description',
        header: 'Omschrijving'
      }
    ];

    this.authService.user$.subscribe(() => {})
    this.patientService.getByUserId().pipe(switchMap((patient) => {
      return this.patientService.getRecords(patient.id);
    })).subscribe((response) => {
      this.recordCases = response;
      this.patient = this.patientService.patient;
    });
  }

  public navToDetail(row: RecordCase): void {
    this.router.navigate([row.id], { relativeTo: this.route });
  }
}

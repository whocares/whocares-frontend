import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { FormFieldComponent } from './form-field/form-field.component';
import { TimelineComponent } from './timeline/timeline.component';
import { SideMenuComponent } from './side-menu/side-menu.component';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
  ],
  declarations: [
    HeaderComponent,
    FormFieldComponent,
    TimelineComponent,
    SideMenuComponent,
  ],
  exports: [
    HeaderComponent,
    FormFieldComponent,
    TimelineComponent,
    SideMenuComponent
  ],
})
export class UiModule {
}

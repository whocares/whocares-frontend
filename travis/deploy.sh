PREVIOUS_IMAGE_NAME=$( kubectl get deployments/"$K8_DEPLOYMENT_NAME" -o=jsonpath='{$.spec.template.spec.containers[:1].image}' | sed 's/^.//;s/.$//' )
SHA=$(git rev-parse HEAD)
NEW_IMAGE_NAME=${REPO_NAME}:${SHA}

docker build -q -t ${REPO_NAME}:latest -t ${NEW_IMAGE_NAME} -f ./docker/patient/Dockerfile .
docker push ${REPO_NAME}:latest
docker push ${NEW_IMAGE_NAME}

echo NEW IMAGE NAME IS "$NEW_IMAGE_NAME"
echo DEPLOYING TO DEPLOYMENT "$K8_DEPLOYMENT_NAME"
echo USING CONTAINER NAME "$K8_CONTAINER_NAME"

kubectl set image deployments/"$K8_DEPLOYMENT_NAME" "$K8_CONTAINER_NAME"="$NEW_IMAGE_NAME"

source travis/post-deploy.sh

import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { ChatConfig } from '../chat-config';
import { filter, map, tap } from 'rxjs/operators';
import { AuthService } from '@whocares/data-auth';
import { Chat, ChatMessage } from '../interfaces/chat-message.interface';
import { WebsocketService } from './websocket.service';

@Injectable({
  providedIn: 'root'
})
export class ChatService {
  public activeChat$: Observable<Chat>;
  public messageAdded$: Observable<ChatMessage>;
  public updateChat = new BehaviorSubject<boolean>(true);

  private messageAdded: BehaviorSubject<ChatMessage> = new BehaviorSubject<ChatMessage>(null);
  private activeChat: BehaviorSubject<Chat> = new BehaviorSubject<Chat>(null);
  private endpoint = 'chats';
  private unreadMessages: number;

  private chats: Partial<Chat>[] = [];

  constructor(
    private websocketService: WebsocketService,
    private http: HttpClient,
    private chatConfig: ChatConfig,
    private authService: AuthService
  ) {
    this.activeChat$ = this.activeChat.asObservable();
    this.messageAdded$ = this.messageAdded.asObservable();
    this.listenForMessages();
  }

  public setActiveChat(chat: Chat): void {
    this.activeChat.next(chat);
  }

  public createChat(users: { user_id: string, scope: 'patient' | 'health-care-provider' }[]): Observable<any> {
    return this.http.post(`${this.chatConfig.endpoint}/chat-service/${this.endpoint}/create`, { chat: { users } }).pipe(tap(() => {
      this.updateChat.next(true);
    }));
  }

  public getAll(): Observable<Chat[]> {
    return this.http.get<{ chats: Chat[] }>(`${this.chatConfig.endpoint}/chat-service/${this.endpoint}`).pipe(map((response) => response.chats));
  }

  public getMessages(chatId: string): Observable<ChatMessage[]> {
    const storedChat = this.chats.find((chat) => {
      return chat.id === chatId;
    });

    if (storedChat) {
      return of(storedChat.messages);
    } else {
      return this.http.get<{ messages: ChatMessage[] }>(`${this.chatConfig.endpoint}/chat-service/messages/fetch-messages/${chatId}`)
        .pipe(
          map((response) => response.messages),
          tap((messages) => {
            const chat = this.chats.find((chat) => chat.id === chatId);
            if (!chat) {
              this.chats = [...this.chats, { id: chatId, messages }];
            }
          })
        );
    }
  }

  public addMessage(text: string, chatId: string): Observable<ChatMessage> {
    return this.http.post<ChatMessage>(`${this.chatConfig.endpoint}/chat-service/messages/send/${chatId}`, { message: { content: text } })
      .pipe(
        tap((message) => {
          let storedChat = this.chats.find((chat) => {
            return chat.id === chatId;
          });

          if (storedChat) {
            storedChat = { ...storedChat, messages: [...storedChat.messages, message] };

            const index = this.chats.findIndex((chat) => storedChat.id === chat.id);
            if (index !== -1) {
              this.chats[index] = storedChat;
            }
          }
        })
      );
  }

  public listenForMessages(): void {
    this.authService.user$.pipe(filter(Boolean)).subscribe((user: any) => {
      this.websocketService.pusher.channel(`private-users.${user.uid}`).bind('App\\Events\\MessageSent', (data) => {
        const message = {...data.message, name: data.name};
        this.messageAdded.next(message);
      });
    });
  }
}

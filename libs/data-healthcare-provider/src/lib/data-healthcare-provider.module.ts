import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HealthcareProviderConfig } from './healthcare-provider.config';

@NgModule({
  imports: [
    CommonModule
  ]
})
export class DataHealthcareProviderModule {
  public static forRoot(config: { endpoint: string }) {
    return {
      ngModule: DataHealthcareProviderModule,
      providers: [
        {
          provide: HealthcareProviderConfig,
          useValue: config
        }
      ]
    };
  }
}

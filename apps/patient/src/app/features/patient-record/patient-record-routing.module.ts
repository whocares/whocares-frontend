import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RecordComponent } from './record/record.component';
import { RecordCaseDetailComponent } from './pages/record-case-detail/record-case-detail.component';

const routes: Routes = [
  {
    path: '',
    component: RecordComponent,
  },
  {
    path: ':id',
    component: RecordCaseDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PatientRecordRoutingModule {
}

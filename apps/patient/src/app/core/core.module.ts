import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppLayoutComponent } from './layout/app-layout/app-layout.component';
import { RouterModule } from '@angular/router';
import { AuthLayoutComponent } from './layout/auth-layout/auth-layout.component';
import { UiModule } from '@whocares/ui';

@NgModule({
  declarations: [
    AppLayoutComponent,
    AuthLayoutComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    UiModule,
  ]
})
export class CoreModule {
}

module.exports = {
  projects: [
    '<rootDir>/apps/patient',
    '<rootDir>/libs/data-auth',
    '<rootDir>/libs/ui',
    '<rootDir>/libs/data-patient',
    '<rootDir>/libs/utils',
    '<rootDir>/apps/healthcare-provider',
    '<rootDir>/libs/data-healthcare-provider',
  ],
};

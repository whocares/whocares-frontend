import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UiModule } from '@whocares/ui';
import { UtilsModule } from '@whocares/utils';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    UiModule,
    UtilsModule,
  ],
  exports: [
    UiModule,
    UtilsModule,
  ],
})
export class SharedModule {
}

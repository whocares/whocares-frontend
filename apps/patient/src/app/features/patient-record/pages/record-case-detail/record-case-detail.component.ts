import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Medication, PatientService, RecordCase, RecordElement } from '@whocares/data-patient';
import { Observable } from 'rxjs';
import { map, shareReplay, switchMap } from 'rxjs/operators';
import { TimelineItem } from '@whocares/ui';

@Component({
  selector: 'whocares-record-case-detail',
  templateUrl: './record-case-detail.component.html',
  styleUrls: ['./record-case-detail.component.css']
})
export class RecordCaseDetailComponent implements OnInit {
  public recordCase$: Observable<RecordCase>;
  public medicineRecords$: Observable<Medication[]>;
  public appointmentRecords$: Observable<RecordElement[]>;
  public columns: any[];
  public peppy = [{ name: 'hey', amount: 1 }];
  public appointmentColumns: any[];
  public timelineItems$: Observable<TimelineItem[]>;

  constructor(private router: Router, private route: ActivatedRoute, private patientService: PatientService) {
  }

  public ngOnInit(): void {
    this.setColumns();
    this.recordCase$ = this.route.params.pipe(switchMap(({ id }) => {
      return this.patientService.getRecord(id);
    }), shareReplay(1));

    this.getMedicineRecords();

    this.timelineItems$ = this.recordCase$.pipe(map((recordCase) => {
      return recordCase.recordElements.map((recordElement) => {
        let title = '';

        switch (recordElement.elementType) {
          case 1:
            title = 'Medicatie toegevoegd';
            break;
          case 2:
            title = 'Afspraak';
            break;
        }
        return {
          title: title,
          content: recordElement.content,
          date: recordElement.createdAt
        };
      });
    }));
  }

  public getMedicineRecords(): void {
    this.medicineRecords$ = this.recordCase$.pipe(map((recordCase) => {
        return recordCase.recordElements.reduce((acc, value) => {
          return [...acc, ...value.medications];
        }, []);
      })
    );

    this.appointmentRecords$ = this.recordCase$.pipe(map((recordCases) => {
      return recordCases.appointments;
    }));
  }

  public setColumns(): void {
    this.columns = [
      {
        field: 'medicine.name',
        header: 'Medicijn'
      },
      {
        field: 'amount',
        header: 'Aantal'
      },
      {
        field: 'usageType',
        header: 'Te gebruiken'
      },
      {
        field: 'startDate',
        header: 'Start datum',
        type: 'date'
      },
      {
        field: 'endDate',
        header: 'Eind datum',
        type: 'date'
      }
    ];

    this.appointmentColumns = [
      {
        field: 'content',
        header: 'Omschrijving'
      },
      {
        field: 'createdAt',
        header: 'Datum',
        type: 'date',
      }
    ];
  }
}

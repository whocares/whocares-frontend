import { Injectable } from '@angular/core';

@Injectable()
export class HealthcareProviderConfig {
  public endpoint: string;
}

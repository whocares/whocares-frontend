export interface HealthcareProvider {
  id: string;
  firstName: string;
  lastName: string;
  type: {
    name: string;
    description: string;
  }
}

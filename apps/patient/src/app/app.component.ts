import { Component } from '@angular/core';
import { ChatService } from '../../../../libs/data-chat/src/lib/services/chat.service';
import { filter } from 'rxjs/operators';
import { MessageService } from 'primeng/api';
import { ChatMessage } from '@whocares/data-chat';

@Component({
  selector: 'whocares-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'patient-frontend';

  constructor(private chatService: ChatService, private toastService: MessageService) {
    this.chatService.messageAdded$.pipe(filter(Boolean)).subscribe((message: ChatMessage) => {
      // @ts-ignore
      this.toastService.add({
        summary: `Nieuw bericht van ${message.name}`,
        detail: message.content,
        severity: 'info'
      });
    });
  }
}

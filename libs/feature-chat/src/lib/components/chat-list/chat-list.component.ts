import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Chat } from '@whocares/data-chat';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '@whocares/data-auth';
import { ChatService } from '../../../../../data-chat/src/lib/services/chat.service';
import { HealthcareProviderService } from '../../../../../data-healthcare-provider/src/lib/services/healthcare-provider.service';
import { Observable } from 'rxjs';
import { SelectItem } from 'primeng/api';
import { map } from 'rxjs/operators';
import { DialogService } from 'primeng/dynamicdialog';
import { AddChatDialogComponent } from '../add-chat-dialog/add-chat-dialog.component';
import { FeatureChatConfig } from '../feature-chat.config';

@Component({
  selector: 'whocares-chat-list',
  templateUrl: './chat-list.component.html',
  styleUrls: ['./chat-list.component.scss'],
  providers: [DialogService]
})
export class ChatListComponent implements OnInit {
  get chats(): Chat[] {
    return this._chats;
  }

  @Input()
  set chats(value: Chat[]) {
    this._chats = value;

    if (value && value.length) {
      this.selectChat(value[0]);
    }
  }

  @Output()
  public onSelectChat = new EventEmitter();
  public activeChat$: Observable<Chat>;

  private _chats: Chat[];

  constructor(
    public config: FeatureChatConfig,
    private http: HttpClient,
    private authService: AuthService,
    private chatService: ChatService,
    private hcpService: HealthcareProviderService,
    private dialogService: DialogService
  ) {
    this.activeChat$ = this.chatService.activeChat$;
  }

  public hcpList$: Observable<SelectItem[]>;

  public ngOnInit(): void {
    this.hcpList$ = this.hcpService.all().pipe(map((hcpList) => {
      return hcpList.map((hcp) => {
        return {
          label: `${hcp.firstName} ${hcp.lastName} - (${hcp.type.name})`,
          value: hcp.id
        };
      });
    }));
  }

  public selectChat(chat: Chat): void {
    this.onSelectChat.emit(chat);
    this.chatService.setActiveChat(chat);
  }

  public openAddChatDialog(): void {
    this.dialogService.open(AddChatDialogComponent, {
      header: 'Start nieuwe chat',
      width: '500px',
    });
  }

}

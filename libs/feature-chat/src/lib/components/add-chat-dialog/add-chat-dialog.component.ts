import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import { HealthcareProviderService } from '../../../../../data-healthcare-provider/src/lib/services/healthcare-provider.service';
import { Observable } from 'rxjs';
import { SelectItem } from 'primeng/api';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ChatService } from '../../../../../data-chat/src/lib/services/chat.service';
import { DynamicDialogRef } from 'primeng/dynamicdialog';
import { FeatureChatConfig } from '../feature-chat.config';
import { PatientService } from '@whocares/data-patient';

@Component({
  selector: 'whocares-add-chat-dialog',
  templateUrl: './add-chat-dialog.component.html',
  styleUrls: ['./add-chat-dialog.component.scss']
})
export class AddChatDialogComponent implements OnInit {
  public hcpList$: Observable<SelectItem[]>;
  public form: FormGroup;
  public userList$: Observable<SelectItem[]>;

  constructor(
    private hcpService: HealthcareProviderService,
    private formBuilder: FormBuilder,
    private chatService: ChatService,
    private dialogRef: DynamicDialogRef,
    private config: FeatureChatConfig,
    private patientService: PatientService,
  ) {
  }

  public ngOnInit(): void {
    this.form = this.formBuilder.group({
      userId: [null, Validators.required]
    });

   const method: Observable<any[]> = this.config.scope === 'patient' ? this.hcpService.all() : this.patientService.all();

      this.hcpList$ = method.pipe(map((hcpList) => {
        return hcpList.map((hcp) => {
          return {
            label: `${hcp.firstName} ${hcp.lastName}`,
            value: hcp.firebaseUserId,
          };
        });
      }));

      this.hcpList$.subscribe((op) => {
        console.log(op);
      })
  }

  public submit(): void {
    if (this.form.valid) {
      this.chatService.createChat([{
        user_id: this.form.value.userId,
        scope: this.config.scope === 'patient' ? 'health-care-provider' : 'patient',
      }]).subscribe(() => {
        this.dialogRef.close();
      });
    }
  }

}

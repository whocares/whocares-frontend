import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '@whocares/data-auth';
import { PatientService } from '@whocares/data-patient';
import { SideMenuItem } from '@whocares/ui';

@Component({
  selector: 'app-layout',
  templateUrl: './app-layout.component.html',
  styleUrls: ['./app-layout.component.scss']
})
export class AppLayoutComponent {
  public menuItems: SideMenuItem[];

  constructor(private authService: AuthService, private router: Router, private patientService: PatientService) {
    this.menuItems = [
      {
        label: 'Dossier',
        icon: 'fas fa-user',
        routerLink: 'patient-record'
      },
      {
        label: 'Chats',
        icon: 'fas fa-users',
        routerLink: 'chat'
      }
    ];

  }

  public logout(): void {
    this.patientService.patient = null;
    this.authService.logout();
  }

}

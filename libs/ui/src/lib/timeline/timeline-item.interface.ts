export interface TimelineItem {
  title: string;
  content: string;
  date: string;
}

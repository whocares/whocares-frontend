import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '@whocares/data-auth';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  public form: FormGroup;
  public loginError: boolean;

  constructor(private formBuilder: FormBuilder, private authService: AuthService, private router: Router) {
    this.form = this.formBuilder.group({
      email: [],
      password: []
    });
  }

  public login(): void {
    this.authService.login(this.form.value.email, this.form.value.password).subscribe((res) => {
      this.router.navigate(['/']);
    }, err => {
      this.loginError = true;
    });
  }

}

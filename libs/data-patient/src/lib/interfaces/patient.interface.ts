export interface Patient {
  id: string;
  firstName: string;
  lastName: string;
  gender: string;
  birthdate: string;
  citizenServiceNumber: string;
}

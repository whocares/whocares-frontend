import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { TimelineItem } from './timeline-item.interface';

@Component({
  selector: 'whocares-timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TimelineComponent {
  @Input()
  public items: TimelineItem[];

}

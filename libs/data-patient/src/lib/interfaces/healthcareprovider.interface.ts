import {Type} from "./type.interface";

export interface Healthcareprovider {
  id: string;
  firstName: string;
  lastName: string;
  type: Type;
}

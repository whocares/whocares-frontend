import { Injectable } from '@angular/core';
import { HealthcareProviderConfig } from '../healthcare-provider.config';
import { Observable, of } from 'rxjs';
import { HealthcareProvider } from '../interfaces/healthcare-provider.interface';
import { HttpClient } from '@angular/common/http';
import { map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HealthcareProviderService {
  private hcpList: HealthcareProvider[];

  constructor(private healthCareProviderConfig: HealthcareProviderConfig, private http: HttpClient) {
  }

  public all(): Observable<HealthcareProvider[]> {
    if (this.hcpList) {
      return of(this.hcpList);
    } else {
      return this.http.get<any>(`${this.healthCareProviderConfig.endpoint}/hcp-service/healthcare-providers`).pipe(
        map((response) => response.healthcareProviders),
        tap((hcplList) => this.hcpList = hcplList),
      );
    }
  }
}

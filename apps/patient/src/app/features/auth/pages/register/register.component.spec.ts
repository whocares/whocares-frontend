import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterComponent } from './register.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { AuthModule } from '../../auth.module';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { AuthMockService, AuthService } from '@whocares/data-auth';
import { PatientService } from '@whocares/data-patient';
import { UiModule } from '@whocares/ui';

describe('RegisterComponent', () => {
  let component: RegisterComponent;
  let fixture: ComponentFixture<RegisterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [],
      providers: [{ provide: AuthService, useClass: AuthMockService }],
      imports: [HttpClientTestingModule, ReactiveFormsModule, AuthModule, RouterTestingModule, UiModule],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call register of authService on submit', () => {
    const authService = fixture.debugElement.injector.get(AuthService);
    const patientService = fixture.debugElement.injector.get(PatientService);
    const registerSpy = spyOn(authService, 'register').and.returnValue(of({ uid: '12345667' }));
    const registerPatientSpy = spyOn(patientService, 'register').and.returnValue(of(null));

    component.form.patchValue({
      firstName: 'test',
      email: 'test@test.test',
      lastName: 'test',
      gender: 'M',
      birthdate: new Date(),
      password: 'Test1234',
      confirmPassword: 'Test1234',
    });

    component.submit();

    expect(registerSpy).toHaveBeenCalled();
    expect(registerPatientSpy).toHaveBeenCalled();
  });

  it('should validate if passwords are the same', () => {
    component.form.patchValue({
      password: 'peppy',
      confirmPassword: 'peppy2',
    });

    expect(component.form.errors.match).toBeTruthy();

    component.form.patchValue({
      password: 'peppy',
      confirmPassword: 'peppy',
    });

    component.form.updateValueAndValidity();

    expect(component.form.errors).toBeNull();
  });

  it('should validate the user input', () => {
    expect(component.form.invalid).toBeTruthy();
    component.form.patchValue({
      name: 'peppy',
    });

    expect(component.form.invalid).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable } from 'rxjs';
import { fromPromise } from 'rxjs/internal-compatibility';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';

export interface AuthProvider {
  login(email: string, password: string): Observable<boolean>;

  logout(): void;

  register(email: string, password: string): Observable<{ uid: string }>;
}

@Injectable({
  providedIn: 'root',
})
export class AuthService implements AuthProvider {
  public user$: Observable<firebase.User>;

  constructor(
    private firebaseAuth: AngularFireAuth,
    private router: Router,
  ) {
    this.user$ = firebaseAuth.authState;
  }

  public login(email: string, password: string): Observable<boolean> {
    return fromPromise(this.firebaseAuth
      .signInWithEmailAndPassword(email, password)).pipe(map((res) => true));
  }

  public logout(): void {
    localStorage.clear();
    this.firebaseAuth.signOut().then(() => {
      this.router.navigate(['/auth/login']);
    });
  }

  /**
   * Register user with fireBase and with patient service
   *
   * @param email
   * @param password
   */
  public register(email: string, password: string): Observable<{ uid: string }> {
    return fromPromise(this.firebaseAuth.createUserWithEmailAndPassword(email, password)).pipe(map((response) => {
      return { uid: response.user.uid };
    }));
  }

  public getPatientId(): string {
    return localStorage.getItem('patientId');
  }
}

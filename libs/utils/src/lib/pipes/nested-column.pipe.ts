import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'nestedColumn'
})
export class NestedColumnPipe implements PipeTransform {
  transform(field: string, dataObject: any): unknown {
    return field.split('.').reduce((acc, value) => {
      return acc[value];
    }, dataObject);
  }
}

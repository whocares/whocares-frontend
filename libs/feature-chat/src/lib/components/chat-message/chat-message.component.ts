import { Component, Input, OnInit } from '@angular/core';
import { ChatMessage } from '@whocares/data-chat';

@Component({
  selector: 'whocares-chat-message',
  templateUrl: './chat-message.component.html',
  styleUrls: ['./chat-message.component.scss']
})
export class ChatMessageComponent implements OnInit {
  @Input()
  public message: ChatMessage;

  constructor() { }

  ngOnInit(): void {
  }

}

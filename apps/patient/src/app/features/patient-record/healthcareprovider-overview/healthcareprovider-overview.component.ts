import { Component, OnInit } from '@angular/core';
import {PatientService} from "@whocares/data-patient";
import {AuthService} from "@whocares/data-auth";
import {Healthcareprovider} from "../../../../../../../libs/data-patient/src/lib/interfaces/healthcareprovider.interface";

@Component({
  selector: 'whocares-healthcareprovider-overview',
  templateUrl: './healthcareprovider-overview.component.html',
  styleUrls: ['./healthcareprovider-overview.component.css']
})
export class HealthcareproviderOverviewComponent implements OnInit {
  public healthcareproviders: Healthcareprovider[];
  public columns: any[];

  constructor(
    private patientService: PatientService,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.authService.user$.subscribe(() => {});
    this.patientService.getHealthcareProviders()
      .subscribe((response) => {
        console.log(JSON.stringify(response));
        this.healthcareproviders = response ;
      });
  }

}

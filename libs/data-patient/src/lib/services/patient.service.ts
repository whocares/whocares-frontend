import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { RegisterRequest } from '../interfaces/register-request.interface';
import { RecordCase } from '../interfaces/record.interface';
import { filter, first, map, switchMap, tap } from 'rxjs/operators';
import { Patient } from '../interfaces/patient.interface';
import { AuthService } from '@whocares/data-auth';
import { Healthcareprovider } from '../interfaces/healthcareprovider.interface';
import { DatePatientConfig } from '../date-patient-config';

@Injectable({
  providedIn: 'root'
})
export class PatientService {
  public patient: Patient;

  constructor(private http: HttpClient, private authService: AuthService, private config: DatePatientConfig) {
  }

  public register(patient: RegisterRequest): Observable<any> {
    const postBody = {};

    for (const key in patient) {
      const snakeCaseKey = key.replace(/[A-Z]/g, letter => `_${letter.toLowerCase()}`);
      postBody[snakeCaseKey] = patient[key];
    }

    return this.http.post(`${this.config.baseUrl}/patient-service/patients`, postBody);
  }

  public getRecords(patientId: string): Observable<RecordCase[]> {
    return this.http.get<{ recordCases: RecordCase[] }>(`${this.config.baseUrl}/patient-service/record-cases/patient/${patientId}`).pipe(
      map((response) => response.recordCases)
    );
  }

  public getRecord(id: string): Observable<RecordCase> {
    return this.http.get<{ recordCase: RecordCase }>(`${this.config.baseUrl}/patient-service/record-cases/${id}`).pipe(
      map((response) => response.recordCase)
    );
  }

  public getByUserId(userId?: string, forceReload?: boolean): Observable<Patient> {
    return (this.patient) ?
      of(this.patient) :
      this.authService.user$.pipe(
        filter(Boolean),
        first(),
        switchMap((user: firebase.User) => {
          return this.http.get<{ patient: Patient }>(`${this.config.baseUrl}/patient-service/patients/user/${user.uid}`);
        }),
        map((response) => response.patient),
        tap((response) => {
          this.patient = response;
        })
      );
  }

  public getHealthcareProviders(): Observable<Healthcareprovider[]> {
    return this.http.get<{ healthcareProviders: Healthcareprovider[] }>(`${this.config.baseUrl}/hcp-service/healthcare-providers`)
      .pipe(map(response => response.healthcareProviders));
  }

  public all(): Observable<Patient[]> {
    return this.http.get<{ patients: Patient[] }>(`${this.config.baseUrl}/patient-service/patients?per_page=99999`).pipe(map((res) => res.patients));
  }
}

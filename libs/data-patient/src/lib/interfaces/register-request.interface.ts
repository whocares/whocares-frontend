export interface RegisterRequest {
  firstName: string;
  lastNamePrefix: string;
  lastName: string;
  gender: string;
  email: string;
  birthdate: string;
  password: string;
  confirmPassword: string;
  userId: string;
}

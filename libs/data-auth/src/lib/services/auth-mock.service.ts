import { AuthProvider } from './auth.service';
import { Observable, of } from 'rxjs';

export class AuthMockService implements AuthProvider {
  public login(email: string, password: string): Observable<boolean> {
    return of(true);
  }

  public logout(): void {
    return;
  }

  public register(email: string, password: string): Observable<{ uid: string }> {
    return of({ uid: '123456789' });
  }
}

import { Component, OnInit } from '@angular/core';
import { PatientService } from '@whocares/data-patient';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(private patientService: PatientService) { }

  ngOnInit(): void {
    this.patientService.getRecord('8e8b4d08-f670-4887-b501-985d02074374').subscribe((res) => {
      console.log(res);
    })
  }

}

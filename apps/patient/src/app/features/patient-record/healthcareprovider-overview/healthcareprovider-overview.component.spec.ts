import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HealthcareproviderOverviewComponent } from './healthcareprovider-overview.component';

describe('HealthcareproviderOverviewComponent', () => {
  let component: HealthcareproviderOverviewComponent;
  let fixture: ComponentFixture<HealthcareproviderOverviewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HealthcareproviderOverviewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HealthcareproviderOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Pipe, PipeTransform } from '@angular/core';
import { Chat } from '@whocares/data-chat';

@Pipe({
  name: 'chatUsers'
})
export class ChatUsersPipe implements PipeTransform {

  public transform(value: Chat): unknown {
    return value.users.map((user) => `${user.firstName} ${user.lastName}`).join(',');
  }

}

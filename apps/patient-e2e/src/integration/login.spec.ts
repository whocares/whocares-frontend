
describe('Login', () => {
  beforeEach(() => cy.visit('/'));

  it('should show an error message on faulty login', () => {
    cy.get('[data-test=email-field]').type('test');
    cy.get('[data-test=password-field]').type('test');
    cy.get('[data-test=login-btn]').click();

    cy.get('[data-test=login-error]').should('exist');
  });

  it('should login when entering the correct credentials', () => {
    cy.fixture('developer').then(({ username, password }) => {
      cy.get('[data-test=email-field]').type(username);
      cy.get('[data-test=password-field]').type(password);

      cy.get('[data-test=login-btn]').click();

      cy.url().should('include', 'patient-record');

      cy.get('[data-test=logout-btn]').click();
    });
  })
});

import { Component } from '@angular/core';
import { ChatMessage, ChatService } from '@whocares/data-chat';
import { MessageService } from 'primeng/api';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'whocares-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'healthcare-provider';

  constructor(private chatService: ChatService, private toastService: MessageService) {
    this.chatService.messageAdded$.pipe(filter(Boolean)).subscribe((message: ChatMessage) => {
      // @ts-ignore
      this.toastService.add({
        summary: `Nieuw bericht van ${message.name}`,
        detail: message.content,
        severity: 'info'
      });
    });
  }
}

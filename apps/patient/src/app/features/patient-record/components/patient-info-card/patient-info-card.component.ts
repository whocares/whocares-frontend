import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Patient } from '@whocares/data-patient';

@Component({
  selector: 'whocares-patient-info-card',
  templateUrl: './patient-info-card.component.html',
  styleUrls: ['./patient-info-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.Default,
})
export class PatientInfoCardComponent {
  @Input()
  public patient: Patient;
}

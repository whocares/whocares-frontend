export interface SideMenuItem {
  label: string;
  routerLink: string;
  icon: string;
}
